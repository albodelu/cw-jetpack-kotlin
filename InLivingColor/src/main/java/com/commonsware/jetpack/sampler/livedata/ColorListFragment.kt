/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.livedata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list.*

class ColorListFragment : Fragment() {
  private lateinit var vm: ColorViewModel

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_list, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    vm = ViewModelProviders
      .of(this, ColorViewModelFactory(savedInstanceState))
      .get()

    val colorAdapter = ColorAdapter(layoutInflater) { color ->
      navTo(color)
    }

    items.apply {
      layoutManager = LinearLayoutManager(activity)
      addItemDecoration(
        DividerItemDecoration(
          activity,
          DividerItemDecoration.VERTICAL
        )
      )
      adapter = colorAdapter
    }

    vm.numbers.observe(this, Observer { colorAdapter.submitList(it) })
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    vm.onSaveInstanceState(outState)
  }

  private fun navTo(color: Int) {
    findNavController().navigate(ColorListFragmentDirections.showColor(color))
  }
}