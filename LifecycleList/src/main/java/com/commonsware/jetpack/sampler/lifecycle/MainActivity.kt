/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.lifecycle

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
  private lateinit var adapter: EventAdapter
  private lateinit var vm: EventViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val items = findViewById<RecyclerView>(R.id.items)

    vm = ViewModelProviders.of(this).get()
    adapter = EventAdapter(layoutInflater, vm.startTime)
    addEvent("onCreate()")

    items.layoutManager = LinearLayoutManager(this)
    items.addItemDecoration(
      DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
    )
    items.adapter = adapter
  }

  override fun onStart() {
    super.onStart()

    addEvent("onStart()")
  }

  override fun onResume() {
    super.onResume()

    addEvent("onResume()")
  }

  override fun onPause() {
    addEvent("onPause()")

    super.onPause()
  }

  override fun onStop() {
    addEvent("onStop()")

    super.onStop()
  }

  override fun onDestroy() {
    addEvent("onDestroy()")

    super.onDestroy()
  }

  private fun addEvent(message: String) {
    vm.addEvent(message, hashCode())
    adapter.submitList(vm.events)
  }
}
