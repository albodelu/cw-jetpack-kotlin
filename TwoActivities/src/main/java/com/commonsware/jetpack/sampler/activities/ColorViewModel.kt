/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.activities

import android.os.Bundle
import androidx.lifecycle.ViewModel
import java.util.*

private const val STATE_NUMBERS = "numbers"

class ColorViewModel(state: Bundle?) : ViewModel() {
  private val random = Random()
  var numbers = state?.getIntegerArrayList(STATE_NUMBERS) ?: buildItems()

  fun onSaveInstanceState(state: Bundle) {
    state.putIntegerArrayList(STATE_NUMBERS, numbers)
  }

  fun refresh() {
    numbers = buildItems()
  }

  private fun buildItems() =
    ArrayList<Int>(generateSequence { random.nextInt() }.take(25).toList())
}