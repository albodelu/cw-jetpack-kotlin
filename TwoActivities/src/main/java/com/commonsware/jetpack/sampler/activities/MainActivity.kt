/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
  private lateinit var vm: ColorViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    vm = ViewModelProviders
      .of(this, ColorViewModelFactory(savedInstanceState))
      .get()

    val colorAdapter = ColorAdapter(layoutInflater)

    items.apply {
      layoutManager = LinearLayoutManager(this@MainActivity)
      addItemDecoration(
        DividerItemDecoration(
          this@MainActivity,
          DividerItemDecoration.VERTICAL
        )
      )
      adapter = colorAdapter.apply {
        submitList(vm.numbers)
      }
    }

    toolbar.apply {
      setTitle(R.string.app_name)
      inflateMenu(R.menu.actions)

      setOnMenuItemClickListener { item ->
        when {
          item.itemId == R.id.refresh -> {
            vm.refresh()
            colorAdapter.submitList(vm.numbers)
            true
          }
          item.itemId == R.id.about -> {
            Toast.makeText(
              this@MainActivity,
              R.string.msg_toast,
              Toast.LENGTH_LONG
            ).show()
            true
          }
          else -> false
        }
      }
    }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    vm.onSaveInstanceState(outState)
  }
}
